@setup
$user = 'root';
$timezone = 'Europe/Moscow';

$path = '/var/www/bot';
$current = $path . '/current';

$repo = "https://ArtemSmirnovb13@bitbucket.org/ArtemSmirnovb13/tgrbt.git";
$branch = 'master';

$chmods = [
'storage/logs'
];

$date = new datetime('now', new DateTimeZone($timezone));

$release = $path . '/releases/'. $date->format('YmdHis');
@endsetup

@servers(['production' => $user . '@45.128.204.190'])

@task('clone', ['on' => $on])
mkdir -p {{$release}}

git clone --depth 1 -b {{$branch}} "{{$repo}}" {{$release}}

echo "#1 - Repository has been cloned"
@endtask

@task('composer', ['on' => $on])
composer self-update

cd {{$release}}

composer install

echo "#2 - Composer dependecies have been installed"
@endtask

@task('artisan', ['on' => $on])
cd {{$release}}

ln -nfs {{$path}}/.env .env;
chgrp -h www-data .env;

php artisan config:clear

echo "#3 - Composer dependecies have been installed"
@endtask

@task('chmod', ['on' => $on])

chmod 777 -R /var/www/bot

echo "#4 - Permissions has been set"
@endtask
//rr
@task('update_symlinks', ['on' => $on])
ln -nfs {{$release}} {{$current}};
chgrp -h www-data {{$current}};

echo "#5 - Symlink has been set"
@endtask

@macro('deploy', ['on' => 'production'])
clone
composer
artisan
chmod
update_symlinks
@endmacro