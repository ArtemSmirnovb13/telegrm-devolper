<?php

namespace App\Telegram;

use App\Http\Controllers\Backend\TelegramController;
use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Keyboard;
/**
 * Class HelpCommand.
 */
class TestCommand extends Command
{
    protected $name = 'start';

    protected $description = 'H123';

    /**
     * {@inheritdoc}
     */
    public function handle()
    {
        $telegram_user = \Telegram::getWebhookUpdates();
        $chat_ID       = $telegram_user->getMessage()->getChat()->getid();

        $telegram_controller = new TelegramController();
        $telegram_controller->saveLastCommand();
     //   file_put_contents('webHook_first.log', 'setDatacomand------'.var_export($chat_ID, true)."------\n\n", FILE_APPEND);
        $this->replyWithChatAction(['action' => Actions::TYPING]);
        $keyboard = [
            ['Погода ☀', 'Новости', 'Скоро'],
            ['Скоро', 'Викторина', 'Рекорды'],
            ['Калькулятор']
        ];
        $reply_markup = \Telegram\Bot\Keyboard\Keyboard::make([
            'keyboard' => $keyboard,
            'resize_keyboard' => true,
            'one_time_keyboard' => false
        ]);
        $response = \Telegram::sendMessage([
            'chat_id'      => $chat_ID,
            'text'         => 'Добро пожаловать. Для начала работы будь добр выбери что нибудь, да или нет, всё просто. Для возврата в главное меню нажмите /start или /help',
            'reply_markup' => $reply_markup
        ]);
        //$this->triggerCommand('calculator');

    }
}