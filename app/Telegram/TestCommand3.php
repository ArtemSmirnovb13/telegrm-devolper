<?php

namespace App\Telegram;

use BotMan\BotMan\BotMan;
use BotMan\BotMan\BotManFactory;
use Telegram\Bot\Commands\Command;
use BotMan\BotMan\Drivers\DriverManager;
/**
 * Class HelpCommand.
 */
class TestCommand3 extends Command
{
    protected $name = 'q';

    protected $description = 'q';

    /** @var Question */
    protected $quizQuestions;

    /** @var integer */
    protected $userPoints = 0;

    /** @var integer */
    protected $userCorrectAnswers = 0;

    /** @var integer */
    protected $questionCount = 0; // we already had this one

    /** @var integer */
    protected $currentQuestion = 1;

    /**
     * {@inheritdoc}
     */
    public function handle()
    {

//        $telegram_user = \Telegram::getWebhookUpdates();
//        $chat_ID       = $telegram_user->getMessage()->getChat()->getid();
//
//        $this->showInfo();



    }

    private function showInfo()
    {
        $telegram_user = \Telegram::getWebhookUpdates();
        $chat_ID       = $telegram_user->getMessage()->getChat()->getid();
        $response      = \Telegram::sendMessage([
            'chat_id' => $chat_ID,
            'text'    => 'You will be shown '.$this->questionCount
                         .' questions about Laravel. Every correct answer will reward you with a certain amount of points. Please keep it fair, and don\'t use any help. All the best! 🍀',

        ]);


    }
}