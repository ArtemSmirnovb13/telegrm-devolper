<?php

namespace App\Conversations;



use App\Highscore;
use BotMan\BotMan\Facades\BotMan;
use BotMan\BotMan\Messages\Conversations\Conversation;
use BotMan\BotMan\Drivers\DriverManager;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Outgoing\Question;

class QuizConversation extends Conversation
{

    protected $quizQuestions;

    /** @var integer */
    protected $userPoints = 0;

    /** @var integer */
    protected $userCorrectAnswers = 0;

    /** @var integer */
    protected $questionCount = 0; // we already had this one

    /** @var integer */
    protected $currentQuestion = 1;
    /**
     * Start the conversation.
     *
     * @return mixed
     */
    public function run()
    {
        $this->quizQuestions = \App\Question::all()->shuffle();
        $this->questionCount = $this->quizQuestions->count();
        $this->quizQuestions = $this->quizQuestions->keyBy('id');
        $this->showInfo();
    }

    private function showInfo()
    {
        $this->say('Вам будет показано'.$this->questionCount.' вопроса о Laravel. Каждый правильный ответ будет вознаграждать вас определенным количеством баллов.. Пожалуйста не юзайте инет и будьте честными , удачи !!!!! 🍀');
        $this->checkForNextQuestion();
    }

    private function checkForNextQuestion()
    {
        if ($this->quizQuestions->count()) {
            return $this->askQuestion($this->quizQuestions->first());
        }

        $this->showResult();
    }

    private function askQuestion(\App\Question $question)
    {
        $this->ask($this->createQuestionTemplate($question), function (Answer $answer) use ($question) {
            $quizAnswer = \App\Answer::find($answer->getValue());

            if (! $quizAnswer) {
                $this->say('Я не понимаю. Пожалуйста используйте кнопки указанные выше.');
                return $this->checkForNextQuestion();
            }

            $this->quizQuestions->forget($question->id);

            if ($quizAnswer->correct_one) {
                $this->userPoints += $question->points;
                $this->userCorrectAnswers++;
                $answerResult = '✅';
            } else {
                $correctAnswer = $question->answers()->where('correct_one', true)->first()->text;
                $answerResult = "❌ (Правильный: {$correctAnswer})";
            }
            $this->currentQuestion++;

            $this->say("Ваш ответ: {$quizAnswer->text} {$answerResult}");
            $this->checkForNextQuestion();
        });
    }

    private function createQuestionTemplate(\App\Question $question)
    {
        $questionText = '➡️ Вопрос: '.$this->currentQuestion.' / '.$this->questionCount.' : '.$question->text;
        $questionTemplate = Question::create($questionText);
        $answers = $question->answers->shuffle();

        foreach ($answers as $answer) {
            $questionTemplate->addButton(Button::create($answer->text)->value($answer->id));
        }

        return $questionTemplate;
    }

    private function showResult()
    {
        $this->say('Конец 🏁');
        $this->say("Ты ответили на все вопросы. И заработали {$this->userPoints} очков ! Правильных ответов: {$this->userCorrectAnswers} / {$this->questionCount}");

        $this->askAboutHighscore(); // this is new in this method
    }

    private function askAboutHighscore()
    {
        $question = Question::create('Хотите, чтобы вас добавили в список рекордов?.')
            ->addButtons([
                Button::create('Да')->value('yes'),
                Button::create('Нет')->value('no'),
            ]);

        $this->ask($question, function (Answer $answer) {
            switch ($answer->getValue()) {
                case 'yes':
                    $rank = Highscore::getRank($this->userPoints);
                    $this->say("БЛЯЯЯЯЯЯЯЯЯЯ {$rank}.");
                    $user = Highscore::saveUser($this->bot->getUser(), $this->userPoints, $this->userCorrectAnswers,$rank);
                    $this->say("Отлично. Твой ранг {$user->rank}.");
                    return $this->bot->startConversation(new HighscoreConversation());
                case 'no':
                    return $this->say('Без проблем =)).  😉');
                default:
                    return $this->repeat('Пожалуйста, используйте кнопки.');
            }
        });
    }


}