<?php

namespace App\Conversations;

use App\Highscore;
use BotMan\BotMan\Messages\Conversations\Conversation;

class HighscoreConversation extends Conversation
{
    /**
     * Start the conversation.
     *
     * @return mixed
     */
    public function run()
    {
        $this->showHighscore();
    }

    private function showHighscore()
    {
        $topUsers = Highscore::topUsers();

        if (! $topUsers->count()) {
            return $this->say('Рекордов нет. Будь первым 👍');
        }

        $topUsers->transform(function ($user) {


            return "{$user->rank} - {$user->name} {$user->points} points";
        });

        $this->say('Текущие рекорды !');
        $this->say('🏆 РЕКОРДЫ 🏆');
        $this->say($topUsers->implode("\n"));
       // $this->say($rank->getRank());
    }
}