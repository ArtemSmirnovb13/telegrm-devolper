<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\TelegramUser;
use Telegram;

class CommandsController extends Controller
{
    protected $telegram_webhook;

    public function __construct()
    {

        $this->telegram_webhook = Telegram::getWebhookUpdates();
    }

    public function Weather()
    {
        $telegram = $this->telegram_webhook;
        $user     = TelegramUser::find($telegram['message']['from']['id']);

        $keyboard     = [
            ['Выход']
        ];
        $reply_markup = \Telegram\Bot\Keyboard\Keyboard::make([
            'keyboard'          => $keyboard,
            'resize_keyboard'   => true,
            'one_time_keyboard' => false
        ]);

        if ($telegram['message']['text'] == 'Погода ☀') {
            \Telegram::sendMessage([
                'chat_id'      => $telegram['message']['chat']['id'],
                'text'         => 'Пожалуйста напишите название вашего города =)',
                'reply_markup' => $reply_markup
            ]);
        } else {
            if (isset($telegram['message']['text']) && $user->update_id != Telegram::getWebhookUpdates()['update_id']) {
                $city = $telegram['message']['text'];
                file_put_contents('webHook_first.log', '------'.$city."------\n\n", FILE_APPEND);
                $url = 'http://api.openweathermap.org/data/2.5/weather?q='.$city
                       .'&units=metric&APPID=78cce3399f57314911e82791894826bb';
                try {
                    $client     = new \GuzzleHttp\Client();
                    $nodeValues = $client->get($url)->getBody()->getContents();
                    $temp       = json_decode($nodeValues, true)['main']['temp'];
                    \Telegram::sendMessage([
                        'chat_id'      => $telegram['message']['chat']['id'],
                        'text'         => 'Температура : '.$temp.' '.'C',
                        'reply_markup' => $reply_markup
                    ]);
                    $user->update_id = Telegram::getWebhookUpdates()['update_id'];
                    $user->save();
                } catch (\Exception $e) {
                    if ($telegram['message']['text'] == 'Выход') {
                        $user->last_command = '/new_start';
                        $user->save();
                        $TelegramController = new   TelegramController();
                        $TelegramController->switchCommand();
                    }
                    else {

                        \Telegram::sendMessage([
                            'chat_id'      => $telegram['message']['chat']['id'],
                            'text'         => 'Город не найден',
                            'reply_markup' => $reply_markup
                        ]);
                    }
                }

            }
        }


    }
}
