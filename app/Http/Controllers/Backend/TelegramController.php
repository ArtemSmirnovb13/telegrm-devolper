<?php

namespace App\Http\Controllers\Backend;

use App\Conversations\HighscoreConversation;
use App\Conversations\QuizConversation;
use App\Http\Controllers\Controller;
use App\TelegramUser;
use BotMan\BotMan\BotMan;
use BotMan\BotMan\BotManFactory;
use BotMan\BotMan\Cache\DoctrineCache;
use Doctrine\Common\Cache\FilesystemCache;
use Telegram;
use Telegram\Bot\Actions;
use Telegram\Bot\Api;

class TelegramController extends Controller
{
    /**
     * @throws Telegram\Bot\Exceptions\TelegramSDKException
     */

    protected $telegram_webhook;

    public function __construct()
    {

        $this->telegram_webhook = Telegram::getWebhookUpdates();
    }

    public function webhook()
    {
        info('incoming', request()->all()); // this line was added

        $config = [
            // Your driver-specific configuration
            "telegram" => [
                "token" => "904293394:AAFH77hW6RUpJfEYvQv_hactkKAr5m4WNwQ"
            ]
        ];
        $doctrineCacheDriver = new FilesystemCache(__DIR__."/cache");
          file_put_contents('webHook_first.log', '------'.var_export($doctrineCacheDriver,true)."------\n\n", FILE_APPEND);
        $botman = BotManFactory::create($config,new DoctrineCache($doctrineCacheDriver));




        $telegram = $this->telegram_webhook;

        if (isset($telegram['callback_query'])) {
            if (!TelegramUser::find($telegram['callback_query']['from']['id'])) {
                TelegramUser::create($telegram['callback_query']['from']);
            }
        } else {
            if (!TelegramUser::find($telegram['message']['from']['id'])) {
                TelegramUser::create($telegram['message']['from']);
            }
        }

        $botman->hears('Викторина', function (BotMan $bot) {

            $bot->startConversation(new QuizConversation());
        });

        $botman->hears('Рекорды', function (BotMan $bot) {
            $bot->startConversation(new HighscoreConversation());
        })->stopsConversation();
        $botman->listen();

            $this->checkCommand();
            sleep(1);
            $this->switchCommand();


//        $update =Telegram::getWebhookUpdates();
//        \Telegram::sendMessage([
//            'chat_id' => $update->getMessage()->getChat()->getId(),
//            'text'    => $this->telegram_webhook->getMessage()->getText()
//        ]);

        Telegram::commandsHandler(true);

        //  file_put_contents('webHook_first.log', '------'.json_encode($arr,true)."------\n\n", FILE_APPEND);
    }

    /**
     *
     */
    public function switchCommand()
    {
        $telegram = $this->telegram_webhook;
        $command  = $this->getLastCommand();


        if ($command == 'Калькулятор') {
            $this->Calc();
        } elseif ($command == '/new_start') {
            $this->startCommand();
        } elseif ($command == 'Погода ☀') {
            $CommandsController = new   CommandsController();
            $CommandsController->Weather();
        }


    }


    public function checkCommand()
    {
        $telegram = $this->telegram_webhook;
        $update =Telegram::getWebhookUpdates();
        //  file_put_contents('webHook_first.log', '------'.$telegram."------\n\n", FILE_APPEND);

        if ($this->getLastCommand() == null) {
            \Telegram::sendMessage([
                'chat_id' => $update->getMessage()->getChat()->getId(),
                'text'    => 'Ошибка - поле last command empty',
            ]);
        } else {
            if ($update->getMessage()->getText() == 'Калькулятор') {
                $this->saveLastCommand();
            } elseif ($update->getMessage()->getText() == '/new_start') {
                $this->saveLastCommand();
            } elseif ($update->getMessage()->getText() == 'Погода ☀') {
                $this->saveLastCommand();
            }
            elseif ($update->getMessage()->getText() == 'Викторина') {
                $this->saveLastCommand();
            }
            elseif ($update->getMessage()->getText() == 'Рекорды') {
                $this->saveLastCommand();
            }

        }


    }

    public function Calc()
    {


        $telegram     = $this->telegram_webhook;
        $keyboard     = [
            ['7', '8', '9', '*'],
            ['4', '5', '6', '/'],
            ['1', '2', '3', '-'],
            ['0', 'C', 'Выход', '+']
        ];
        $reply_markup = \Telegram\Bot\Keyboard\Keyboard::make([
            'keyboard'          => $keyboard,
            'resize_keyboard'   => true,
            'one_time_keyboard' => false
        ]);

        $user = TelegramUser::find($telegram['message']['from']['id']);
        if ($user->calc == null) {
            $response        = \Telegram::sendMessage([
                'chat_id'      => $chat_id = $telegram['message']['chat']['id'],
                'text'         => 'Добро пожаловать. Вы вошли в калькулятор',
                'reply_markup' => $reply_markup
            ]);
            $arr             = [
                'a'    => 'null',
                'b'    => 'null',
                'znak' => 'null'
            ];
            $user->calc      = json_encode($arr, true);
            $user->update_id = Telegram::getWebhookUpdates()['update_id'];
            $user->save();
        }
        if ($telegram['message']['text'] == 'Выход') {
            $user->last_command = '/new_start';
            $user->calc         = null;
            $user->save();
            $this->switchCommand();
        }
        if ($telegram['message']['text'] == 'C') {
            $arr             = [
                'a'    => 'null',
                'b'    => 'null',
                'znak' => 'null'
            ];
            $user->calc      = json_encode($arr, true);
            $user->update_id = Telegram::getWebhookUpdates()['update_id'];
            $user->save();
            $response = \Telegram::sendMessage([
                'chat_id'      => $chat_id = $telegram['message']['chat']['id'],
                'text'         => 'Cleared',
                'reply_markup' => $reply_markup
            ]);
        }
        $array = json_decode($user->calc, true);
        if ($array['a'] != 'null') {
            if ($array['znak'] != 'null') {
                if ($array['b'] != 'null') {
                    $operator = $array['znak'];
                    switch ($operator) {
                        case '+':
                            $response        = \Telegram::sendMessage([
                                'chat_id'      => $chat_id = $telegram['message']['chat']['id'],
                                'text'         => $array['a'] + $array['b'],
                                'reply_markup' => $reply_markup
                            ]);
                            $arr             = [
                                'a'    => 'null',
                                'b'    => 'null',
                                'znak' => 'null'
                            ];
                            $user->calc      = json_encode($arr, true);
                            $user->update_id = Telegram::getWebhookUpdates()['update_id'];
                            $user->save();
                            break;
                        case '-':
                            $response        = \Telegram::sendMessage([
                                'chat_id'      => $chat_id = $telegram['message']['chat']['id'],
                                'text'         => $array['a'] - $array['b'],
                                'reply_markup' => $reply_markup
                            ]);
                            $arr             = [
                                'a'    => 'null',
                                'b'    => 'null',
                                'znak' => 'null'
                            ];
                            $user->calc      = json_encode($arr, true);
                            $user->update_id = Telegram::getWebhookUpdates()['update_id'];
                            $user->save();
                            break;
                        case '*':
                            $response        = \Telegram::sendMessage([
                                'chat_id'      => $chat_id = $telegram['message']['chat']['id'],
                                'text'         => $array['a'] * $array['b'],
                                'reply_markup' => $reply_markup
                            ]);
                            $arr             = [
                                'a'    => 'null',
                                'b'    => 'null',
                                'znak' => 'null'
                            ];
                            $user->calc      = json_encode($arr, true);
                            $user->update_id = Telegram::getWebhookUpdates()['update_id'];
                            $user->save();
                            break;
                        case '/':
                            $response        = \Telegram::sendMessage([
                                'chat_id'      => $chat_id = $telegram['message']['chat']['id'],
                                'text'         => $array['a'] / $array['b'],
                                'reply_markup' => $reply_markup
                            ]);
                            $arr             = [
                                'a'    => 'null',
                                'b'    => 'null',
                                'znak' => 'null'
                            ];
                            $user->calc      = json_encode($arr, true);
                            $user->update_id = Telegram::getWebhookUpdates()['update_id'];
                            $user->save();
                            break;
                    }
                } else {
                    if (isset($telegram['message']['text'])
                        && $user->update_id != Telegram::getWebhookUpdates()['update_id']
                    ) {

                        $array['b']      = $telegram['message']['text'];
                        $user->calc      = json_encode($array, true);
                        $user->update_id = Telegram::getWebhookUpdates()['update_id'];
                        $user->save();
                        $this->Calc();
                    }
                }

            } else {
                if (isset($telegram['message']['text'])
                    && $user->update_id != Telegram::getWebhookUpdates()['update_id']
                ) {

                    $array['znak']   = $telegram['message']['text'];
                    $user->calc      = json_encode($array, true);
                    $user->update_id = Telegram::getWebhookUpdates()['update_id'];
                    $user->save();
                    $this->Calc();
                }
            }

        } else {
            if (isset($telegram['message']['text']) && $user->update_id != Telegram::getWebhookUpdates()['update_id']) {

                $array['a']      = $telegram['message']['text'];
                $user->calc      = json_encode($array, true);
                $user->update_id = Telegram::getWebhookUpdates()['update_id'];
                $user->save();
                $this->Calc();
            }
        }
    }

    public function setDataCommand()
    {
        $telegram = $this->telegram_webhook;
        //    file_put_contents('webHook_first.log', '------'.$telegram."------\n\n", FILE_APPEND);
        if (isset($telegram['callback_query'])) {
            $user    = TelegramUser::find($telegram['callback_query']['from']['id']);
            $chat_id = $telegram['callback_query']['message']['chat']['id'];
        } else {
            $user    = TelegramUser::find($telegram['message']['from']['id']);
            $chat_id = $telegram['message']['chat']['id'];
        }

        if ($user->json == null) {
            $arr        = [
                'name'  => 'null',
                'phone' => 'null'

            ];
            $user->json = json_encode($arr, true);
            $user->save();
        }
        $array = json_decode($user->json, true);
        file_put_contents('webHook_first.log', 'setDatacomand------'.var_export($array, true)."------\n\n",
            FILE_APPEND);

        $data = $this->getLastCommand();

        switch ($data) {
            case "Указать":

                if ($array['name'] != 'null') {
                    if ($array['phone'] != 'null') {
                        $param    = [
                            'inline_keyboard' => [
                                [
                                    ['text' => "Стереть данные", 'callback_data' => "ResetData"],
                                    ['text' => "Главное меню", 'callback_data' => "/start"]
                                ]
                            ],
                        ];
                        $response = \Telegram::sendMessage([
                            'chat_id'      => $chat_id,
                            'text'         => 'Всё отлично, больше нет необходимых данных '.'Ваше указанное имя: '
                                              .$array['name'].' И указанный номер телефона : '.$array['phone']
                                              .' При необходимости вы можете ввести данные повторно. ',
                            'reply_markup' => $this->inlineKeyBoard($param)
                        ]);
                    } else {


                        if (isset($telegram['message']['text'])
                            && $user->update_id != Telegram::getWebhookUpdates()['update_id']
                        ) {

                            $array['phone'] = $telegram['message']['text'];
                            $user->json     = json_encode($array, true);
                            $user->save();
                            $this->setDataCommand();
                        }
                        if ($array['phone'] == 'null') {
                            $response = \Telegram::sendMessage([
                                'chat_id' => $chat_id,
                                'text'    => 'Пожалуйста, укажите ваш номер телефона'
                            ]);
                        }
                    }
                } else {
                    file_put_contents('webHook_first.log',
                        'setDatacomand------'.Telegram::getWebhookUpdates()['update_id']."------\n\n", FILE_APPEND);
                    if (isset($telegram['message']['text'])) {

                        $array['name']   = $telegram['message']['text'];
                        $user->json      = json_encode($array, true);
                        $user->update_id = Telegram::getWebhookUpdates()['update_id'];
                        $user->save();
                        $this->setDataCommand();
                    }
                    if ($array['name'] == 'null') {
                        $response = \Telegram::sendMessage([
                            'chat_id' => $chat_id,
                            'text'    => 'Пожалуйста, укажите ваше имя'
                        ]);
                    }

                }
                break;
            case "ResetData":

                $arr        = [
                    'name'  => 'null',
                    'phone' => 'null'

                ];
                $user->json = json_encode($arr, true);
                $user->save();
                $response = \Telegram::sendMessage([
                    'chat_id' => $chat_id,
                    'text'    => 'Данные стёрты'

                ]);
                $param    = [
                    'inline_keyboard' => [
                        [
                            ['text' => "Указать данные", 'callback_data' => "Указать"],
                            ['text' => "Главное меню", 'callback_data' => "/start"]
                        ]
                    ],
                ];

                $response = \Telegram::sendMessage([
                    'chat_id'      => $chat_id,
                    'text'         => 'Пожалуйста укажите ваше имя и номер телефона',
                    'reply_markup' => $this->inlineKeyBoard($param)
                ]);

                break;
            default:
                $param = [
                    'inline_keyboard' => [
                        [
                            ['text' => "Указать данные", 'callback_data' => "Указать"],
                            ['text' => "Главное меню", 'callback_data' => "/start"]
                        ]
                    ],
                ];

                $response = \Telegram::sendMessage([
                    'chat_id'      => $chat_id,
                    'text'         => 'Пожалуйста укажите ваше имя и номер телефона',
                    'reply_markup' => $this->inlineKeyBoard($param)
                ]);
        }

        //  file_put_contents('webHook_first.log', '------'.$name."------\n\n", FILE_APPEND);

        // $array = json_decode($user->json, true);

    }

    public function test()
    {
        $keyboard = [
            'inline_keyboard' => [
                [['text' => "Да", 'callback_data' => "Yes"], ['text' => "Ещё раз да", 'callback_data' => "Yes"]],
                [['text' => "Нет", 'callback_data' => "No"]],
                [['text' => "Навэльный", 'callback_data' => "navalny"]]
            ],
        ];
        $markup   = json_encode($keyboard, true);
        $telegram = Telegram::getWebhookUpdates();
        $message  = $telegram->getMessage()->gettext();
        $chat_ID  = $telegram->getMessage()->getChat()->getid();

        if (isset($telegram['callback_query'])) {
            $callback_query = $telegram['callback_query'];
            $data           = $callback_query['data'];
            //   file_put_contents('webHook_first.log', '------'.$data."------\n\n", FILE_APPEND);
        } else {
            switch ($message) {
                case 'ok':
                    \Telegram::sendMessage([
                        'chat_id'      => $chat_ID,
                        'text'         => 'Нажми уже что нибудь',
                        'reply_markup' => $markup
                    ]);
                    break;
                default:
                    if ($message != '/start') {
                        \Telegram::sendMessage([
                            'chat_id'      => $chat_ID,
                            'text'         => 'Ты не нажал ни чего',
                            'reply_markup' => $markup
                        ]);
                    }
            }
        }

        if (isset($telegram['callback_query'])) {
            $callback_query = $telegram['callback_query'];
            $data           = $callback_query['data'];
            switch ($data) {
                case 'Yes':
                    \Telegram::sendMessage([
                        'chat_id'      => $chat_ID,
                        'text'         => 'Ты нажал ДАААААа',
                        'reply_markup' => $markup
                    ]);

                    break;
                case 'No':
                    \Telegram::sendMessage([
                        'chat_id'      => $chat_ID,
                        'text'         => 'Ты нажал Нееет',
                        'reply_markup' => $markup
                    ]);
                    break;
                case 'navalny':

                    \Telegram::sendMessage([
                        'chat_id' => $chat_ID,
                        'text'    => '......................',
                    ]);

                    $action = new Api(\Telegram::getAccessToken());
                    //Бот печатает
                    $action->sendChatAction(
                        [
                            'chat_id' => $chat_ID,
                            'action'  => Actions::TYPING
                        ]
                    );
                    sleep(10);
                    \Telegram::sendMessage([
                        'chat_id' => $chat_ID,
                        'text'    => 'охохо да ты опасен',
                    ]);
                    $action = new Api(\Telegram::getAccessToken());
                    //Бот печатает
                    $action->sendChatAction(
                        [
                            'chat_id' => $chat_ID,
                            'action'  => Actions::TYPING
                        ]
                    );
                    sleep(10);
                    $photo
                              = \Telegram\Bot\FileUpload\InputFile::create('https://cdn1.img.inosmi.ru/images/24150/63/241506342.jpg');
                    $telegram = new Api(\Telegram::getAccessToken());


                    $telegram->sendPhoto([
                        'chat_id' => $chat_ID,
                        'photo'   => $photo,

                    ]);
                    break;
            }
        }

        //file_put_contents('webHook_first.log', '------'.$telegram."------\n\n", FILE_APPEND);
        file_put_contents('webHook_first.log', 'Message is: ------'.$message."------\n\n", FILE_APPEND);
    }

    public function getLastCommand()
    {
        $telegram = $this->telegram_webhook;
        if (isset($telegram['callback_query'])) {

            $user = TelegramUser::find($telegram['callback_query']['from']['id']);
            return $user->last_command;
        } else {

            $user = TelegramUser::find($telegram['message']['from']['id']);
            return $user->last_command;
        }
    }

    public function saveLastCommand()
    {
        $telegram = $this->telegram_webhook;
        if (isset($telegram['callback_query'])) {
            $user               = TelegramUser::find($telegram['callback_query']['from']['id']);
            $last_command       = $telegram['callback_query']['data'];
            $user->last_command = $last_command;
            $user->save();
        } else {
            $user               = TelegramUser::find($telegram['message']['from']['id']);
            $last_command       = $telegram['message']['text'];
            $user->last_command = $last_command;
            $user->save();
        }


    }

    public function inlineKeyBoard($param)
    {
        $keyboard = $param;
        $markup   = json_encode($keyboard, true);
        return $markup;
    }

    /**
     * @throws Telegram\Bot\Exceptions\TelegramSDKException
     */
    public function startCommand()
    {
        $telegram_user = \Telegram::getWebhookUpdates();
        $chat_ID       = $telegram_user->getMessage()->getChat()->getid();

        $action = new Api(\Telegram::getAccessToken());
        //Бот печатает
        $action->sendChatAction(
            [
                'chat_id' => $chat_ID,
                'action'  => Actions::TYPING
            ]
        );
        $keyboard     = [
            ['Погода ☀', 'Новости', 'Скоро'],
            ['Скоро', 'Викторина', 'Рекорды'],
            ['Калькулятор']
        ];
        $reply_markup = \Telegram\Bot\Keyboard\Keyboard::make([
            'keyboard'          => $keyboard,
            'resize_keyboard'   => true,
            'one_time_keyboard' => false
        ]);
        $response     = \Telegram::sendMessage([
            'chat_id'      => $chat_ID,
            'text'         => 'Добро пожаловать. Для начала работы будь добр выбери что нибудь, да или нет, всё просто. Для возврата в главное меню нажмите /start или /help',
            'reply_markup' => $reply_markup
        ]);
    }
}
