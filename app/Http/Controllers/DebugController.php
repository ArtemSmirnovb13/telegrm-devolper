<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\TelegramUser;
use Telegram;
use Telegram\Bot\Actions;
use Telegram\Bot\Api;

class DebugController extends Controller
{
    public function index (){

      // dd(file_get_contents('webHook_first.log',FILE_APPEND));
        $client = new \Goutte\Client();
        $crawler = $client->request('GET',
            'https://kpfu.ru/week_sheadule_print?p_date=03.09.19&p_group=&p_group_name=2161121&p_sub=6241')
            ->filter("table");

        $nodeValues = $crawler->each(
            function (\Symfony\Component\DomCrawler\Crawler $node, $i) {
                echo $node->html();
            }

        );
    }
}
