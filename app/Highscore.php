<?php

namespace App;

use BotMan\BotMan\Interfaces\UserInterface;
use Illuminate\Database\Eloquent\Model;

class Highscore extends Model
{
    protected $fillable = ['chat_id', 'name', 'points', 'correct_answers', 'tries', 'rank'];
    protected $table    = 'highscore';

    public static function saveUser(UserInterface $botUser, int $userPoints, int $userCorrectAnswers, int $userRank)
    {
        $user = static::updateOrCreate(['chat_id' => $botUser->getId()], [
            'chat_id'         => $botUser->getId(),
            'name'            => $botUser->getFirstName().' '.$botUser->getLastName(),
            'points'          => $userPoints,
            'correct_answers' => $userCorrectAnswers,
            'rank' => $userRank,
        ]);

        $user->increment('tries');


        $user->save();

        return $user;
    }

    public static function getRank($userPoints)
    {
        return static::query()->where('points', '>', $userPoints)->pluck('points')->unique()->count() + 1;
    }

    public static function topUsers($size = 10)
    {
        return static::query()->orderByDesc('points')->take($size)->get();
    }
}
