<?php

use Illuminate\Database\Seeder;
use App\Question;
use App\Answer;
class QuestionAnswerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Question::truncate();
        Answer::truncate();
        $questionAndAnswers = $this->getData();

        $questionAndAnswers->each(function ($question) {
            $createdQuestion = Question::create([
                'text' => $question['question'],
                'points' => $question['points'],
            ]);

            collect($question['answers'])->each(function ($answer) use ($createdQuestion) {
                Answer::create([
                    'question_id' => $createdQuestion->id,
                    'text' => $answer['text'],
                    'correct_one' => $answer['correct_one'],
                ]);
            });
        });
    }

    private function getData()
    {
        return collect([
            [
                'question' => 'Кто создатель Laravel?',
                'points' => '5',
                'answers' => [
                    ['text' => 'Christoph Rumpel', 'correct_one' => false],
                    ['text' => 'Jeffrey Way', 'correct_one' => false],
                    ['text' => 'Taylor Otwell', 'correct_one' => true],
                ],
            ],
            [
                'question' => 'Which of the following is a Laravel product?',
                'points' => '10',
                'answers' => [
                    ['text' => 'Horizon', 'correct_one' => true],
                    ['text' => 'Sunset', 'correct_one' => false],
                    ['text' => 'Nightfall', 'correct_one' => true],
                ],
            ],
            [
                'question' => 'Сколько будет 2 + 2',
                'points' => '1044',
                'answers' => [
                    ['text' => '2', 'correct_one' => true],
                    ['text' => '4', 'correct_one' => false],
                    ['text' => '6', 'correct_one' => false],
                ],
            ],
            [
                'question' => 'Что такое Homestead ',
                'points' => '431',
                'answers' => [
                    ['text' => 'Сервер', 'correct_one' => false],
                    ['text' => 'Пакет включающий в себя приложения для разработки', 'correct_one' => true],
                    ['text' => 'Сервис по отладке вэб приложений', 'correct_one' => false],
                ],
            ],
            [
                'question' => 'Что такое Middleware ',
                'points' => '431',
                'answers' => [
                    ['text' => 'Посредники предоставляют удобный механизм для фильтрации HTTP-запросов вашего приложения.', 'correct_one' => true],
                    ['text' => 'Посредники (англ. middleware) предоставляют удобный механизм защиты.', 'correct_one' => false],
                    ['text' => 'Посредники для обработки данных', 'correct_one' => false],
                ],
            ],
        ]);
    }
}
